#include "bit.h"

Bit  Bit::operator+(const Bit& b) const {
    if (*this == bitval::ONE && b == bitval::ONE)   return Bit(bitval::ONE);
    if (*this == bitval::ZERO && b == bitval::ZERO) return Bit(bitval::ZERO);
    if (*this == bitval::NONE) return Bit(b.val);
    return Bit(bitval::MIXED);
}

std::ostream& operator<<(std::ostream& os, const Bit& b) {
    if (b == bitval::ONE)        os << 1;
    else if (b == bitval::ZERO)  os << 0;
    else if (b == bitval::MIXED) os << 'm';
    else                         os << 'n';
    return os;
}
