#include <assert.h>
#include <math.h>
#include <fstream>
#include <string.h>
#include <algorithm>
#include <ctime>
#include <random>

#include "bitmap_utils.h"
#include "constants.h"
#include "quadtree.h"

static char* INPUT_DATA_FILE = NULL;
static char* GENERATION_OUTPUT_FILE = NULL;

inline void my_assert(bool val) {
    if (!val) std::cout << "Assertion falied!\n";
}

void run_tests() {
    // merge test
    {
        BitVector x({0,0,0,1,1,0,1,1,1,1,1});
        BitVector y({0,0,1,1,1,0,1,1,1,0,0});
        my_assert((x + y) == BitVector({0,0,-1,1,1,0,1,1,1,-1,-1}));
    }
    // comparison of bitvectors true
    {
        BitVector x({1,0,0,0});
        BitVector y({1,0,0,1});
        my_assert(!(x > y));
        my_assert(x < y);
    }
    // comparison of bitvectors false
    {
        BitVector x({1,1,1,1});
        BitVector y({1,1,1,1});
        my_assert(!(x > y));
        my_assert(!(x < y));
        my_assert(!(y > x));
        my_assert(!(y < x));
        my_assert(y == x);
    }
    // qsize all same
    {
        BitVector z({1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1});
        my_assert(bitmap_utils::qsize(z, 0, z.len()) == 1);
    }
    // qsize
    {
        BitVector z({1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1});
        my_assert(bitmap_utils::qsize(z, 0, z.len()) == 5);
    }
    // qsize
    {
        BitVector z({1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0});
        my_assert(bitmap_utils::qsize(z, 0, z.len()) == 16);
    }
    // tree size
    {
        merged_bitvectors arr;
        int L = 8;
        for (int i = 0; i < L; ++i) {
            BitVectorMerged b(0, L);
            b[i] = 1;
            arr.push_back(b);
        }
        QuadTree tree = QuadTree(arr);

        my_assert(tree.calculate_depth() == 4);
        my_assert(tree.calculate_tree_size() == 22);
        my_assert(tree.calculate_actual_size() == 22);
    }
}

bitvectors generate_vectors(size_t N, size_t length, gen_type type, char* output_file) {
    srand(time(NULL));
    bitvectors values;

    for (size_t i = 0; i < N; ++i) {
        std::vector<Bit> v(length, 0);

        int start = 0;
        for (int size: ATTRIBUTES_SIZES) {
            if (type == gen_type::RANDOM) {
                 for (size_t m = 0; m < size; ++m) { v[start + m] = (rand() % 2) ? 1 : 0; }
            }
            else if (type == gen_type::INTERVAL_ENCODING) {
                int k = rand() % size;
                v[start + k] = 1;
            }
            else if (type == gen_type::RANGE_ENCODING) {
                int k = rand() % size;
                for (size_t m = 0; m < k; ++m) { v[start + m] = 1; }
            }
            start += size;
        }
        values.push_back(v);
    }

    if (SORTED_ORDER) {
        std::sort(values.begin(), values.end());
    }

    std::cout << "Generated " << N << " indexes of " << length << " length\n";
    return values;
}

bitvectors load_vectors_from_file(std::string filename, data_format format,
                                  size_t vect_length) {
    bitvectors values;
    std::ifstream data(filename);
    std::string line;
    while (getline(data, line)) {
        if (format == data_format::INDEXES) {
            std::vector<Bit> vect;
            for (char ch : line) vect.push_back(Bit(ch - '0'));
            if (vect.size() > 0) values.push_back(BitVector(vect));
        } /*else {
            std::vector<Bit> vect(vect_length);
            for (size_t i = std::stoi(line); i > 0; --i) vect[i] = 1;
            values.push_back(vect);
        }*/
    }
    data.close();
    std::cout << "Loaded indexes from file\n";
    return values;
}

const static std::string gen_type_param("--gen_type");
const static std::string source_param("--source");
const static std::string tests_param("--with-tests");
const static std::string length_param("--index_length");
const static std::string n_param("--n");
const static std::string cluster_size_param("--cluster_size");
const static std::string cube_size_param("--cube_size");
const static std::string gen_output_param("--gen_output");
const static std::string data_format_param("--data_format");
const static std::string no_reorder_param("--no_reordering");
const static std::string sorted_param("--sorted");

const static std::string random_gen("random");
const static std::string range_gen("range");
const static std::string interval_gen("interval");

const static std::string raw_format("raw");
const static std::string index_format("indexes");

void parse_cmd_params(int argc, char* argv[]) {
    for (int i = 1; i < argc; ++i) {
        if ((argv[i] == gen_type_param) && (argc > i + 1)) {
            if (argv[i + 1] == random_gen)   GENERATION_TYPE = gen_type::RANDOM;
            if (argv[i + 1] == range_gen)    GENERATION_TYPE = gen_type::RANGE_ENCODING;
            if (argv[i + 1] == interval_gen) GENERATION_TYPE = gen_type::INTERVAL_ENCODING;
        }
        else if ((argv[i] == source_param) && (argc > i + 1)) {
            SHOULD_GENERATE_INPUT = false;
            INPUT_DATA_FILE = argv[i + 1];
        }
        else if ((argv[i] == data_format_param) && (argc > i + 1)) {
            if (argv[i + 1] == raw_format)   INPUT_DATA_FORMAT = data_format::RAW;
            if (argv[i + 1] == index_format) INPUT_DATA_FORMAT = data_format::INDEXES;
        }
        else if (argv[i] == tests_param)      SHOULD_RUN_TESTS = true;
        else if (argv[i] == sorted_param)     SORTED_ORDER = true;
        else if (argv[i] == no_reorder_param) SHOULD_REORDER_ON_PROXIMITY = false;
        else if ((argv[i] == gen_output_param) && (argc > i + 1)) GENERATION_OUTPUT_FILE = argv[i + 1];

        else if ((argv[i] == cluster_size_param) && (argc > i + 1)) CLUSTER_SIZE = atoi(argv[i + 1]);
        else if ((argv[i] == cube_size_param) && (argc > i + 1)) CUBE_SIZE = atoi(argv[i + 1]);
        else if ((argv[i] == length_param) && (argc > i + 1)) INDEX_LENGTH = atoi(argv[i + 1]);
        else if ((argv[i] == n_param) && (argc > i + 1)) INDEXES_N = atoi(argv[i + 1]);
    }
}

int build_quadtree() {
    int total_size = 0;
    auto values = SHOULD_GENERATE_INPUT ? generate_vectors(INDEXES_N, INDEX_LENGTH,
                                                           GENERATION_TYPE, GENERATION_OUTPUT_FILE)
                                        : load_vectors_from_file(INPUT_DATA_FILE, INPUT_DATA_FORMAT,
                                                                 VECTOR_LENGTH);

    if (SHOULD_PRINT_GENERATED_DATA) for (auto& v : values) std::cout << v << "\n";
    if (SHOULD_REORDER_ON_PROXIMITY) std::sort(values.begin(), values.end());

    std::cout << "Index length: " << INDEX_LENGTH
              << "\t Data size: " << INDEXES_N << "\n";

    std::cout << "Clusters: " << INDEXES_N / CLUSTER_SIZE
              << "\t\t Cluster size: " << CLUSTER_SIZE << "\n";

    std::cout << "Cubes: " << INDEXES_N / CLUSTER_SIZE / CUBE_SIZE
              << "\t\t Cube size: " << CUBE_SIZE << "\n\n";

    int cubes_n = INDEXES_N / CLUSTER_SIZE / CUBE_SIZE;
    for (int k = 0; k < cubes_n; ++k) {
        auto clusters = bitmap_utils::cluster_bitvectors(values, k * CUBE_SIZE * CLUSTER_SIZE,
                                                         (k + 1) * CUBE_SIZE * CLUSTER_SIZE,
                                                         CLUSTER_SIZE, SHOULD_REORDER_ON_PROXIMITY);
        auto cubes = bitmap_utils::cube_bitvectors(clusters, SHOULD_REORDER_ON_PROXIMITY);
        //bitmap_utils::print_results(clusters, values);

        QuadTree q(cubes);
        //std::cout << q << "\n\n";
        total_size += q.calculate_actual_size();
    }

    print_results();

    int full_index = 0;
    for (int size : ATTRIBUTES_SIZES) full_index += size;

    std::cout << total_size * 1. / 1024 << " " << " bytes instead of " 
              << INDEXES_N * INDEX_LENGTH / 8 << " bytes\n"
              << "ratio " << INDEXES_N * full_index * 1. / 8 / total_size << " ";
    return total_size;
}

int main(int argc, char* argv[]) {

    if (SHOULD_RUN_TESTS) run_tests();
    parse_cmd_params(argc, argv);

    int min_size = INDEXES_N * INDEX_LENGTH / 8;
    int max_size = 0;
    float avg_size = 0;

    if (!DO_MULTIPLE_EXPERIMENTS) EXPERIMENTS_NUMBER = 1;
    for (int i = 0; i < EXPERIMENTS_NUMBER; ++i) {
        int size = build_quadtree();
        if (size < min_size) min_size = size;
        if (size > max_size) max_size = size;
        avg_size += size;
    }

    if (DO_MULTIPLE_EXPERIMENTS) {
        avg_size /= EXPERIMENTS_NUMBER;
        std::cout << "\nPerformed " << EXPERIMENTS_NUMBER << " experiments.\n"
                  << "Minimum size of tree " << min_size << "\n"
                  << "Maximum size of tree " << max_size << "\n"
                  << "Average size " << (int) avg_size << "\n";
    }

    int l;
    std::cin >> l;

    return 0;
}
