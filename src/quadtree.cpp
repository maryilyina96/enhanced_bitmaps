#include "quadtree.h"

std::vector<Quad*> Quad::get_children() const {
    if (is_leaf()) return { };
    else if (is_vertical_leaf())   return { topLeft, botLeft  };
    else if (is_horizontal_leaf()) return { topLeft, topRight };
    else return { topLeft, topRight, botLeft, botRight };
}

bool Quad::is_leaf() const {
    return ((topLeft == NULL) && (topRight == NULL) &&
            (botLeft == NULL) && (botRight == NULL));
}

bool Quad::is_vertical_leaf() const {
    return ((qsize.x == 1) && (qsize.y > 1));
}

bool Quad::is_horizontal_leaf() const {
    return ((qsize.x > 1) && (qsize.y == 1));
}

bool Quad::can_be_merged() const {
    return (!is_leaf() && (value == 0 || value == 1));
}

void Quad::try_merge() {
    if (can_be_merged()) {
        //depth -= topLeft->depth;
        delete_children();
    }
    else {
        for (auto& ch : get_children()) ch->try_merge();
    }
}

void Quad::delete_children() {
    if (is_horizontal_leaf()) { free(topLeft); free(topRight); }
    else if (is_vertical_leaf())   { free(topLeft); free(botLeft); }
    else if (!is_leaf()) {
        free(topLeft); free(topRight); free(botLeft); free(botRight);
    }
    init();
}

void Quad::create_children() {
    topLeft  = new Quad(depth + 1);
    if (qsize.x > 1) topRight = new Quad(depth + 1);
    if (qsize.y > 1) botLeft  = new Quad(depth + 1);
    if ((qsize.x > 1) && (qsize.y > 1))
        botRight = new Quad(depth + 1);
}

void Quad::insert(Bit _val, position _pos, position qpos, int qw, int qh) {
    value = value + _val;
    qsize = sizes(qw, qh);
    if ((qw == 1) && (qh == 1))
        return;

    Quad* subquad;
    if (!has_been_visited) create_children();
    has_been_visited = true;

    int x, y, w, h;
    if (is_horizontal_leaf())    { h = 1; w = qw / 2; }
    else if (is_vertical_leaf()) { w = 1; h = qh / 2; }
    else { w = qw / 2; h = qh / 2; }

    if (_pos.x < qpos.x + w) {
        x = qpos.x;
        if (_pos.y < qpos.y + h) {
            subquad = topLeft; y = qpos.y;
        }
        else {
            subquad = botLeft; y = qpos.y + h;
        }
    } else {
        x = qpos.x + w;
        if (_pos.y < qpos.y + h) {
            subquad = topRight; y = qpos.y;
        }
        else {
            subquad = botRight; y = qpos.y + h;
        }
    }

    subquad->insert(_val, _pos, position(x, y), w, h);
}

void QuadTree::insert(Bit value, position position) {
    root->insert(value, position, Point(0, 0), width, height);
}

QuadTree::QuadTree(merged_bitvectors bitvector_arr) : root(new Quad(1)),
            width(bitvector_arr[0].len()), height(bitvector_arr.size()) {
    for (int i = 0; i < bitvector_arr.size(); ++i) {
        for (int j = 0; j < bitvector_arr[i].len(); ++j) {
            insert(bitvector_arr[i][j], position(j, i));
        }
    }
    check_for_merges();
}

int QuadTree::get_size_of_leaf() {
    /*int power = 0;
    int size = CLUSTER_SIZE - 1;
    while (size > 0) {
        size /= 2;
        power++;
    }
    return ceil(power * 1./ 8);*/
    return ceil(CLUSTER_SIZE * 1. / 2 / 8);
}

int QuadTree::calculate_depth() const {
    int depth = root->get_depth();
    int w(width), h(height);
    while (!((w <= 1) && (h <= 1))) {
        depth++;
        w /= 2; h /= 2;
    }
    return depth;
}

int QuadTree::calculate_tree_size() const {
    int n_on_the_level = 1, size = 0;
    int w(width), h(height);
    while (!((w <= 1) && (h <= 1))) {
        size += n_on_the_level;
        if (w > 1) n_on_the_level *= 2;
        if (h > 1) n_on_the_level *= 2;
        w /= 2; h /= 2;
    }
    size += n_on_the_level;
    return ceil(size * QUADTREE_NODE_SIZE * 1./ 8);
}

static double aver_depth = 0;
static int max_depth = 0;
static int mixed_leafs_cnt = 0;
static int qtree_size = 0;

void print_results() {
    int cubes_n = INDEXES_N / CLUSTER_SIZE / CUBE_SIZE;

    int full_index = 0;
    for (int size : ATTRIBUTES_SIZES) full_index += size;
    int leafs_n = cubes_n * CUBE_SIZE * full_index;

    std::cout << "Average depth of the tree " 
              << aver_depth / cubes_n << " out of " << max_depth 
              << " (" << aver_depth / cubes_n / max_depth * 100 << "%)"<< "\n"

              << "Query time on average "
              << log(aver_depth / cubes_n) * cubes_n << "\n" 

              << "Mixed values in total: " << mixed_leafs_cnt
              << " ("
              << mixed_leafs_cnt * 100. / leafs_n << "%) "
              << qtree_size * 1. / 1024 << " kB\n\n";
}

int QuadTree::calculate_actual_size() const {
    int leafs_size = 0;

    std::queue<Quad*> quads;
    Quad* current;
    quads.push(root);

    int nodes_cnt = 0;
    int depth_sum = 0;
    while (!quads.empty()) {
        current = quads.front();
        quads.pop();

        nodes_cnt += 1;
        if (current->is_leaf()) {
            depth_sum += current->get_depth() * current->nodes_inside();
            if (current->get_value() == bitval::MIXED) leafs_size++;
        }
        for (auto& ch : current->get_children()) quads.push(ch);
    }

    aver_depth += depth_sum * 1. / (width * height);

    if (max_depth == 0) max_depth = calculate_depth();
    mixed_leafs_cnt += leafs_size;

    /*std::cout << "Average depth of the node "
              << depth_sum * 1. / (width * height)
              << " out of max " << calculate_depth() << "\n";*/
    leafs_size *= get_size_of_leaf();
    int tree_size = calculate_tree_size();
    qtree_size += tree_size;

    /*std::cout << "Number of nodes in QuadTree: " << nodes_cnt << "\n"
              << "QuadTree size: " << tree_size << " bytes\n"
              << "Mixed leafs size: " << leafs_size << " bytes\n"
              << "Total size required: " << tree_size + leafs_size << "\n";*/
    return (tree_size + leafs_size);
}

std::ostream& operator<<(std::ostream& os, const QuadTree& tree) {
    std::queue<Quad*> quads;
    Quad* current;
    quads.push(tree.root);

    while (!quads.empty()) {
        current = quads.front();
        std::cout << current->get_value()
                  << ((current->is_leaf()) ? ". " : " ");
        quads.pop();

        for (auto& ch : current->get_children()) quads.push(ch);
    }
    return os;
}
