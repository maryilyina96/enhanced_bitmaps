
#include "bitvector_merged.h"

BitVectorMerged BitVectorMerged::operator+(const BitVectorMerged& v) const {
    if (&v == this) return BitVectorMerged(v);
    BitVector z(static_cast<BitVector>(*this) + static_cast<BitVector>(v));

    const BitVectorMerged* first, *second;
    // The vectors are merged in ascending order
    if (*this > v) { first = &v; second = this; }
    else { first = this; second = &v; }

    // First all ids of the smaller bitvector, then all ids of the greater
    std::vector<size_t> z_ids;
    z_ids.insert(z_ids.end(), first->ids.cbegin(),  first->ids.cend());
    z_ids.insert(z_ids.end(), second->ids.cbegin(), second->ids.cend());

    return BitVectorMerged(z, z_ids);
}

std::ostream& operator<<(std::ostream& os, const BitVectorMerged& v) {
    os << static_cast<BitVector>(v) << " (";
    for (auto id : v.get_ids()) os << id << " ";
    os << ")";
    return os;
}
