#include "bitvector.h"
#include <algorithm>

/* init_val = 2, length = 5 -> 00010
*  init_val = 5, length = 10 -> 0000000110
*/
BitVector::BitVector(unsigned long long init_val, size_t length) : body(length, 0) {
    if (init_val == 0) return;
    for (size_t i = 0; i < this->len(); ++i) {
        body[this->len() - i - 1] = init_val % 2;
        init_val = init_val >> 1;
    }
}

/* RANGE_ENCODING,    11110000 -> return 3
*  INTERVAL_ENCODING, 01000000 -> return 1
*/
int BitVector::decode() const {
    int cnt = 0;
    bitval stop_value;
    if (GENERATION_TYPE == gen_type::RANGE_ENCODING) stop_value = bitval::ZERO;
    if (GENERATION_TYPE == gen_type::INTERVAL_ENCODING) stop_value = bitval::ONE;
    for (auto& b : body) { if (b == stop_value) break; cnt++; }
    return cnt;
}

std::ostream& operator<<(std::ostream& os, const BitVector& v) {
    for (size_t i = 0; i < v.len(); ++i) os << v[i];
    return os;
}

BitVector operator+(const BitVector& v, const BitVector& u) {
    BitVector z(0, std::min(v.len(), u.len()));
    for (size_t i = 0; i < z.len(); ++i) { z[i] = v[i] + u[i]; }
    return z;
}

bool operator>(const BitVector& v, const BitVector& u) {
    for (size_t i = 0; i < v.len(); ++i) {
        if (u[i] < v[i]) return true;
        else if (v[i] < u[i]) return false;
    }
    return false;
}

bool operator<(const BitVector& v, const BitVector& u) {
    for (size_t i = 0; i < v.len(); ++i) {
        if (u[i] > v[i]) return true;
        else if (v[i] > u[i]) return false;
    }
    return false;
}
