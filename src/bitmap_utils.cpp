#include <queue>
#include <unordered_set>
#include <chrono>
#include <math.h>
#include <algorithm>

#include "bitmap_utils.h"

typedef std::chrono::microseconds ms;
typedef std::chrono::high_resolution_clock timer;

long count_in_ms(std::chrono::time_point<timer> start,
                 std::chrono::time_point<timer> finish) {
    return std::chrono::duration_cast<ms>(finish - start).count();
}

double count_in_seconds(std::chrono::time_point<timer> start,
                        std::chrono::time_point<timer> finish) {
    return count_in_ms(start, finish) * 1. / 1000;
}

// for counting timings in all steps
static int time_building_queue = 0;
static int time_taking_from_queue = 0;
static int time_proxim = 0;

int bitmap_utils::qsize(const BitVector& z, int from, int to) {
    if (to - from == 1) return 1;
    int len = to - from;
    auto value = z[from];
    for (int i = from; i < to; ++i) {
        if (z[i] != value)
            return (qsize(z, from, from + len / 2) + qsize(z, to - len / 2, to));
    }
    return 1;
}

int bitmap_utils::hamm_dist(const BitVector& x, const BitVector& y) {
    int res = 0;
    for (int i = 0; i < x.len(); ++i) {
        if (x[i] != y[i]) res++;
    }
    return res;
}

int bitmap_utils::qdistance(const BitVector& x, const BitVector& y) {
    return qsize(x + y, 0, x.len()) * hamm_dist(x, y) * hamm_dist(x, y);
}

merged_bitvectors bitmap_utils::group_bitvectors(const merged_bitvectors& data,
                                                 size_t partition_size) {
    merged_bitvectors partitioned_data;
    for (size_t i = 0; i < data.size(); ++i) {
        if (i % partition_size == 0)
            partitioned_data.push_back(BitVectorMerged(data[i], i));
        else partitioned_data[i / partition_size] =
                partitioned_data[i / partition_size] + BitVectorMerged(data[i], i);
    }
    return partitioned_data;
}

merged_bitvectors bitmap_utils::cluster_bitvectors(const bitvectors& data, size_t from, size_t to,
                                        size_t cluster_size, bool on_proximity) {
    merged_bitvectors clustered_data;
    for (size_t i = from; i < to; ++i)
        clustered_data.push_back(BitVectorMerged(data[i], i));

    return on_proximity ? group_bitvectors_by_proximity(clustered_data, cluster_size)
                        : group_bitvectors(clustered_data, cluster_size);
}

merged_bitvectors bitmap_utils::cube_bitvectors(const merged_bitvectors& clusters,
                                                 bool on_proximity) {
    return on_proximity ? group_bitvectors_by_proximity(clusters)
                        : group_bitvectors(clusters);
}

merged_bitvectors bitmap_utils::group_bitvectors_by_proximity(const merged_bitvectors& data,
                                                              size_t partition_size) {
    auto t1 = timer::now();
    merged_bitvectors partitioned_data;
    for (size_t i = 0; i < data.size(); ++i)
        partitioned_data.push_back(BitVectorMerged(data[i], i));

    /* While we don't get the needed partition_size for partitioned_data,
    *  we continue with merging steps that make the size of data two times smaller.
    */
    while (partitioned_data.size() * partition_size > data.size()) {
        merged_bitvectors new_data;
        proximity_merge_step(partitioned_data, new_data);
        partitioned_data = new_data;
    }

    if (SHOULD_COUNT_TOTAL_TIME) {
        auto t2 = timer::now();
        std::cout << "Elapsed time for partitioning: "
                  << count_in_seconds(t1, t2) << "s\n";
    }
    return partitioned_data;
}

merged_bitvectors bitmap_utils::proximity_merge_step(const merged_bitvectors& data,
                                                     merged_bitvectors& new_data) {
    size_t cnt = 0;
    size_t N = data.size();
    std::vector<bool> taken(N, 0);

    // Repeat until all initial bitvectors are taken
    while (cnt < N) {
        std::vector<size_t> left_out; // inefficient - should be improved
        for (int i = 0; i < N; ++i) {
            if (!taken[i]) left_out.push_back(i);
        }
        auto t1 =  timer::now();
        auto queue = build_queue_for_pairs(left_out, data);
        auto t2 =  timer::now();

        while (!queue.empty() && (cnt < N)) {
            auto pair = queue.top();
            queue.pop();

            if (taken[pair.id1] || taken[pair.id2] || (pair.id1 == pair.id2)) continue;

            taken[pair.id1] = 1; taken[pair.id2] = 1;
            cnt += 2;

            new_data.push_back(data[pair.id1] + data[pair.id2]);
        }

        auto t3 =  timer::now();
        time_building_queue += count_in_ms(t1, t2);
        time_taking_from_queue += count_in_ms(t2, t3);
    }
    return new_data;
}

// Just for testing. Printed the queue to the std output
void print_queue(bitvector_queue pairs, const merged_bitvectors& data) {
    std::vector<std::vector<BitVector>> groupped_by_proximity;
    int prev_dist = 100.;
    std::vector<int> taken(data.size(), 0);
    std::cout << "Queue " << pairs.size() << "\n";

    while (!pairs.empty()) {
        auto top = pairs.top();
        pairs.pop();
        if (taken[top.id1] || taken[top.id2]) continue;

        if (top.qdist != prev_dist)
            groupped_by_proximity.push_back(std::vector<BitVector>());

        prev_dist = top.qdist;

        taken[top.id1] = 1;
        taken[top.id2] = 1;
        groupped_by_proximity[groupped_by_proximity.size() - 1].push_back(data[top.id1]);
        groupped_by_proximity[groupped_by_proximity.size() - 1].push_back(data[top.id2]);
    }

    int sum = 0;
    for (auto& group : groupped_by_proximity) {
        if (group.size() > 0) std::cout << group.size() << "\n";
        sum += group.size();
        for (auto& vect : group) std::cout << vect << "\n";
    }
    std::cout << "\nSum " << sum << "\n";
}

bitvector_queue bitmap_utils::build_queue_for_pairs(const std::vector<size_t>& indexes,
                                                    const merged_bitvectors& data) {
    bitvector_queue pairs;
    int N = indexes.size();
    for (size_t i = 0; i < N; ++i) {
        for (size_t j = i + 1; j < N; ++j) {
            int qdist = bitmap_utils::qdistance(data[indexes[i]], data[indexes[j]]);
            pairs.push(bitvector_pair(indexes[i], indexes[j], qdist));
        }
    }
    //print_queue(pairs, data);
    return pairs;
}

void bitmap_utils::print_results(const merged_bitvectors& clusters,
                                 const bitvectors& values) {
    if (SHOULD_PRINT_MERGED_CLUSTERS) {
        std::cout << "Clusters:\n";
        for (auto& cluster : clusters) {
            std::cout << "\t" << (BitVector) cluster << "\n";
            if (SHOULD_PRINT_CLUSTERS_DATA) {
                for (auto& id : cluster.get_ids())
                    std::cout << (BitVector) values[id] << "\n";
            }
        }
    }
    if (SHOULD_PRINT_SIZES) {
        size_t total = 0;
        for (auto& cluster : clusters) {
            std::cout << "Size " << cluster.get_ids().size() << "\n";
            total += cluster.get_ids().size();
        }
        std::cout << total << "\n";
    }

    if (SHOULD_COUNT_MIXED) {
        int cnt = 0;
        int val = 0;
        for (auto& v: clusters) {
            for (size_t i = 0; i < v.len(); ++i) {
                val++; if (v[i] == bitval::MIXED) cnt++;
            }
        }
        std::cout << "\nMixed values in clusters: " <<
                     cnt * 1. / val * 100 << "% out of " << val << "\n";
    }

    if (SHOULD_COUNT_TOTAL_TIME) {
        std::cout << "building queue " << time_building_queue << "\n" <<
            "taking out " << time_taking_from_queue << "\n" <<
            "proxim " << time_proxim << "\n";
    }
}


bitvectors bitmap_utils::recreate_bitvectors(const merged_bitvectors& clusters,
                                             const bitvectors& data, size_t offset) {
    bitvectors result;
    for (auto& cluster : clusters) {
        result.push_back(cluster);

        // print reordered sequence
        /*for (size_t vect_id : clusters[id].get_ids())
            //std::cout << data[vect_id].decode() << "\n";
            std::cout << data[vect_id] << "\n";
        */
    }
    return result;
}

