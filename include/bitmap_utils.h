#pragma once

#include <queue>
#include "bitvector.h"
#include "bitvector_merged.h"

/*
 * Utility data structure for comparing pairs of bitvectors in priority queue
*/
struct bitvector_pair {
    size_t id1;
    size_t id2;
    int qdist;

    bool operator<(const bitvector_pair& p) const {
        return (qdist > p.qdist);
    }
    bitvector_pair() {}
    bitvector_pair(size_t i, size_t j, int dist) : id1(i), id2(j), qdist(dist) {}
};

/*
 * Bitvectors that we use on cubing stage
 */
typedef std::vector<BitVectorMerged> merged_cubes;

/*
* Queue for comparison of bitvectors in pairs
*/
typedef std::priority_queue<bitvector_pair> bitvector_queue;

namespace bitmap_utils {
    // Calculates hamming distance between two bitvectors
    int hamm_dist(const BitVector& x, const BitVector& y);
    // Calculates the number of quadtree blocks needed to represent the given vector
    int qsize(const BitVector& z, int from, int to);
    // Calculates quadtree distance metric between two bitvectors
    int qdistance(const BitVector& x, const BitVector& y);

    // Performs one merging step - reduces the number of values by half
    merged_bitvectors proximity_merge_step(const merged_bitvectors& initial,
                                           merged_bitvectors& merged);

    // Builds the priority queue of pairs for given bitvectors
    bitvector_queue build_queue_for_pairs(const std::vector<size_t>& indexes,
                                          const merged_bitvectors& data);

    // Merges input bitvectors into groups from original order (on proximity order or not)
    merged_bitvectors group_bitvectors(const merged_bitvectors& data,
                                       size_t partition_size = 1);
    merged_bitvectors group_bitvectors_by_proximity(const merged_bitvectors& data,
                                                    size_t partition_size = 1);

    // Group bitvectors into clusters or cubes, on proximity or original order
    merged_bitvectors cluster_bitvectors(const bitvectors&, size_t from, size_t to,
                                         size_t clusters_max_n, bool on_proximity);
    merged_bitvectors cube_bitvectors(const merged_bitvectors&, bool on_proximity);

    // Prints partitioning results to the standard output
    void print_results(const merged_bitvectors&, const bitvectors&);

    // Reorders bitvectors list according to the formed clusters and cubes
    bitvectors recreate_bitvectors(const merged_bitvectors& clusters,
                                   const bitvectors& data,
                                   size_t offset);
};

