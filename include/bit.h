#pragma once

#include <iostream>

enum class bitval {
    ONE,
    ZERO,
    MIXED,
    NONE // Unitialized value (used for quadtree building)
};

/*
 * Class for three-value bit, bitval::NONE by default
 */
class Bit {
public:
    Bit() {}
    Bit(const Bit& b) : val(b.val) {}
    Bit(bitval v) : val(v) {}
    Bit(int v) { if (v == 1)  val = bitval::ONE;
                 if (v == 0)  val = bitval::ZERO;
                 if (v == -1) val = bitval::MIXED; }
    ~Bit() {}

    bool operator!=(const Bit& b) const { return val != b.val; }
    bool operator==(const Bit& b) const { return val == b.val; }

   /* Comparison is true only for 1 > 0 and 0 < 1.
    * Not applicable to mixed values.
    */
    bool operator>(const Bit& b) const {
        return (val == bitval::ONE && b == bitval::ZERO);
    }

    bool operator<(const Bit& b) const {
        return (val == bitval::ZERO && b == bitval::ONE);
    }

    bool is_set() const { return !(val == bitval::NONE); }

    /* Adding rules:
     * 0 + 0 = 0
     * 1 + 1 = 1
     * 0 + 1 = m
     * m + any = m
     * n + any = any
     */
    Bit operator+(const Bit& b) const;

    friend std::ostream& operator<<(std::ostream& os, const Bit& b);

private:
    bitval val = bitval::NONE;
};
