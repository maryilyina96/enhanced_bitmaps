#pragma once

#include <iostream>

/*
 * Maximum of pairs for each bitvector to be put in the queue
 * If N is greater than N_BASELINE, then only N_BASELINE bitvectors from N will be taken randomly
 */

const static size_t DEFAULT_VECTOR_LENGTH = 10;
static size_t VECTOR_LENGTH = DEFAULT_VECTOR_LENGTH;

static bool SHOULD_RUN_TESTS = false;
// Defines if bitvectors should be generated in program (true) or taken from some file (false)
static bool SHOULD_GENERATE_INPUT = true;

// Perform multiple experiments to get an average result
static bool DO_MULTIPLE_EXPERIMENTS = false;
static int  EXPERIMENTS_NUMBER = 30;

/* Defines if generated values should be sorted lexicographically 
 * before clustering and cubing; true gives better result
 */
static bool SORTED_ORDER = true;
// Defines if block sorting should be performed
static bool SHOULD_REORDER_ON_PROXIMITY = true;

// For input from file. Use INDEXES
enum class data_format { RAW, INDEXES };
static data_format INPUT_DATA_FORMAT = data_format::INDEXES;

enum class gen_type { RANGE_ENCODING, INTERVAL_ENCODING, RANDOM };
// Encoding for generated bitvectors
static gen_type GENERATION_TYPE = gen_type::INTERVAL_ENCODING;
// Number of bins for each attribute, the sum should be equal to INDEX_LENGTH
static int ATTRIBUTES_SIZES[] = { 32, 32, 32, 32 };

// All should be powers of 2 (!)
// Number of entries in the dataset
static size_t INDEXES_N = 16384;
// Length of each row
static size_t INDEX_LENGTH = 128;
static size_t CLUSTER_SIZE = 32;
static size_t CUBE_SIZE = 128;

// Configurations for output
const static bool SHOULD_PRINT_SIZES = false;
const static bool SHOULD_COUNT_MIXED = true;
const static bool SHOULD_PRINT_GENERATED_DATA = false;
const static bool SHOULD_COUNT_TIME_PER_STEP = false;
const static bool SHOULD_COUNT_TOTAL_TIME = false;
const static bool SHOULD_PRINT_MERGED_CUBES = false;
const static bool SHOULD_PRINT_MERGED_CLUSTERS = true;
const static bool SHOULD_PRINT_CLUSTERS_DATA = false;
