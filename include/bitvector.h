#pragma once

#include <vector>
#include <bitset>

#include "bit.h"
#include "constants.h"

class BitVector;
typedef std::vector<BitVector> bitvectors;

/*
 * Vector of three-value bits
 */
class BitVector {
public:
    typedef typename std::vector<Bit> vect_body;
    typedef typename vect_body::iterator iterator;

    BitVector() : BitVector(0) {}
    BitVector(const vect_body& v) : body(v) {}

    // initialises a bitvector in binary format from decimal init_val
    BitVector(unsigned long long init_val) : BitVector(init_val, DEFAULT_VECTOR_LENGTH) {}
    BitVector(unsigned long long init_val, size_t length);
    
    size_t len() const { return body.size(); }
    // restores value, which represents the bitvector (for output to the file and FB tests) 
    int decode() const;
    
    const Bit operator[](size_t id) const { return body[id]; }
    Bit&      operator[](size_t id)       { return body[id]; }
    bool      operator==(const BitVector& v) const { return body == v.body; }

    friend std::ostream& operator<<(std::ostream& os, const BitVector& v);

    iterator begin() noexcept { return body.begin(); }
    iterator end() noexcept   { return body.end(); }

    friend BitVector operator+(const BitVector& v, const BitVector& u);
    friend bool      operator>(const BitVector& v, const BitVector& u);
    friend bool      operator<(const BitVector& v, const BitVector& u);

private:
    vect_body body;
};

