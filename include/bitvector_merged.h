#pragma once

#include "bitvector.h"

class BitVectorMerged;
typedef std::vector<BitVectorMerged> merged_bitvectors;

/*
 * Vector for uniting several BitVectors. Stores ids of initial vectors and merged value.
 */
class BitVectorMerged : public BitVector {
public:
    BitVectorMerged() : BitVector() {}
    BitVectorMerged(const BitVectorMerged& v) : BitVector(v), ids(v.ids) {}
    BitVectorMerged(int init_val) : BitVector(init_val) {}
    BitVectorMerged(int init_val, size_t length) : BitVector(init_val, length) {}

    BitVectorMerged(BitVector v, size_t id) : BitVector(v) { add_id(id); }
    BitVectorMerged(BitVector v, const std::vector<size_t>& ids) : BitVector(v), ids(ids) {}

    void add_id(size_t i) { ids.push_back(i); }
    std::vector<size_t> get_ids() const { return ids; }
    // number of merged bitvectors
    size_t n_of_merges() const { return ids.size(); }

    BitVectorMerged operator+(const BitVectorMerged& v) const;
    friend std::ostream& operator<<(std::ostream& os, const BitVectorMerged& v);

private:
    // ids of bitvectors that were merged together inside this BitVectorMerged object
    std::vector<size_t> ids;
};
