#pragma once

#include <queue>
#include <math.h>

#include "bitvector_merged.h"

struct Point {
    int x, y;
    Point(int _x, int _y): x(_x), y(_y) {}
    Point(): Point(0, 0) {}
};

typedef Point position;
typedef Point sizes;

/*
 * Node of QuadTree, has 1 value - 0, 1 or mixed and pointers to 4 children
 *
 *    topLeft  |  topRight
 *  ------------------------
 *    botLeft  |  botRight
 *
 * If Quad is a leaf, it doesn't have any children.
 * If Quad is a vertical leaf,   it has only topLeft and botLeft,
 *         is a horisontal leaf, it has only topLeft and botLeft.
 *
 */
class Quad {
    // Subquads
    Quad* topLeft;
    Quad* botLeft;
    Quad* topRight;
    Quad* botRight;

    // Value of quads
    Bit value;
    // Width and height of the quad
    sizes qsize;
    // Number of steps from the root to the current quad
    int depth;
    // Flag needed for quad building: if not set (first time visiting) then create children 
    bool has_been_visited;

public:
    Quad(int d) : value(bitval::NONE), has_been_visited(0), depth(d) { init(); }
    ~Quad() { delete_children(); }

    void init() {
        topLeft = NULL; botLeft = NULL; topRight = NULL; botRight = NULL;
    }

    Bit get_value() const { return value; }
    int get_depth() const { return depth; }
    int nodes_inside() const { return qsize.x * qsize.y; }
    std::vector<Quad*> get_children() const;

    bool is_leaf() const;
    bool is_vertical_leaf() const;
    bool is_horizontal_leaf() const;

    // Checks if children have the same values and can be removed
    bool can_be_merged() const;
    void try_merge();
    void delete_children();

    // Used for initialising the node, if it is visited for the first time
    void create_children();

    /* Inserts _val on _pos into the node by descending until the needed leaf is found.
     * qw and qh, qpos - characteristics of current node, calculated by parent
     */
    void insert(Bit _val, position _pos, position qpos, int qw, int qh);

    friend std::ostream& operator<<(std::ostream& os, const Quad& quad) {
        std::cout << quad.value;
        return os;
    }
};

// Memory size per node in bits
const static int QUADTREE_NODE_SIZE = 2;

/*
 * Class for QuadTree
 * Begins from root node and finishes on leafs with width and height equal to 1.
 * Width and height should be the powers of 2.
 * Is build from bitvectors array by inserting each value on its position.
 */
class QuadTree {
private:
    Quad* root;
    int width;
    int height;

public:
    QuadTree(merged_bitvectors bitvector_arr);
    ~QuadTree() { free(root); }

    // Inserts value on position into the tree by descending until the needed leaf is found
    void insert(Bit value, position position);
    // Looks for the blocks that can be merged into one
    void check_for_merges() { root->try_merge(); }

    /* Calculates memory size that is needed for the QTree structure, in bytes
     * - depends only on its width and height, not on the number of mixed values
     */
    int calculate_tree_size() const;

    // Calculates overall memory size in bytes, including tree size and mixed leaves values
    int calculate_actual_size() const;
    
    // Calculate quadtree max depth - the number of steps from the root to the very last leaf
    int calculate_depth() const;
    static int get_size_of_leaf();

    friend std::ostream& operator<<(std::ostream& os, const QuadTree& tree);
};

void print_results();
